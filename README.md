Šią užduotį atliko Diana Makarova.

### Paleidimas

Atsidaryti index.html failą per Google Chrome (ar kitą naršyklę).

### Užduotis

Iš nuorodos 'https://backend.daviva.lt/API/InformacijaTestui' gauti informaciją, iš kurios automobilį reikia pateikti su nuotraukų galerija, paspaudus mygtuką pridėti kitą automobilį.