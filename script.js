var carouselId = 1;
$(document).ready(function() {

    function createCarousel() {
        var carousel = $("<div>");
        $(carousel).addClass('carousel slide');
        $(carousel).attr("data-ride", "carousel");
        $(carousel).append($("<div>").addClass("carousel-inner"));
        $(carousel).attr('id', 'carousel-' + carouselId);

        return carousel;
    }

    function createCarouselItem(url) {
        var carouselItem = $("<div>").addClass("carousel-item");
        $(carouselItem).append($("<img>").addClass("d-block").attr("src", url).attr("alt", "masina"));
        return carouselItem;
    }

    var outputDiv = $('<div class="cars-wrapper"/>');

    function getCar() {
        $(".spinner-border").removeClass("hidden").addClass("shown");
        $(".add-car").addClass("hidden");

        $.getJSON('https://backend.daviva.lt/API/InformacijaTestui', function(data) {

                innerCar = $("<div class='car'>");

                if (data.nuotraukos.length > 0) {
                    var carousel = createCarousel();
                    for (i = 0; i < data.nuotraukos.length; i++) {
                        var carouselItem = createCarouselItem(data.nuotraukos[i]);
                        if (i === 0) {
                            carouselItem.addClass('active');
                        }

                        $(carousel).find(".carousel-inner").append(carouselItem);
                    }
                    $(carousel)
                        .append($('<a>').addClass('carousel-control-prev').attr("role", "button").attr("data-slide", "prev").attr('href', '#carousel-' + carouselId)
                            .append($("<span>").addClass("carousel-control-prev-icon").attr("aria-hidden", "true")))
                        .append($('<a>').addClass('carousel-control-next').attr("role", "button").attr("data-slide", "next").attr('href', '#carousel-' + carouselId)
                            .append($("<span>").addClass("carousel-control-next-icon").attr("aria-hidden", "true")));
                }

                $(innerCar).append(carousel);
                $(innerCar)
                    .append($("<div class='brand'>").append(data.marke))
                    .append($("<div class='model'>").append("Modelis: " + data.modelis))
                    .append($("<div class='year'>").append("Metai: " + data.metai))
                    .append($("<div class='price'>").append(data.kaina + " €"));

                $(outputDiv).append(innerCar);
                carouselId++;

            })
            .done(function() {
                $(".spinner-border").removeClass("shown").addClass("hidden");
                $(".add-car").removeClass("hidden");
                $('.error').removeClass("shown").addClass("hidden");
            })
            .fail(function() {
                $('.error').removeClass("hidden").addClass("shown");
            });

        $(outputDiv).appendTo('.cars-holder');
    }

    getCar();

    $('.add-car').on('click', function() {
        getCar();
    });
});